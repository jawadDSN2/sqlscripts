def ask_for_number_sequence(message):
    elem = input(message)
    return [int(elem) for elem in elem.split(".")]

def is_valid_ip_address(numberlist):
    elements = numberlist.split(".")
    result = True
    for elem in elements:
        num = int(elem)
        if not (num >= 0 and num <= 255):
            result = False
        if not ( numberlist.count(".") == 3):
            result = False
            
    return result

def is_valid_netmask(ip):
    checking_once = True
    ip_bin = ""
    elements = ip.split(".")
    result = True
    
    for elem in elements:
        num = int(elem)
        if not (num >= 0 and num <= 255):
            result = False
        if not ( ip.count(".") == 3):
            result = False
            
        for i in elements:
           ip_bin = ip_bin + f"{int(i):08b}"
           
        for x in ip_bin:
            if x == "1" and checking_once == True:
                result = True
            elif x == "0":
                checking_once = False
                result = True
            elif checking_once == False and x == "1":
                result = False
           

    return result
    
def one_bits_in_netmask(masklijst):
    counter = 0
    ip_bin = ""
    for i in masklijst:
           ip_bin = ip_bin + f"{int(i):08b}"
    for x in ip_bin:
         if x == "1":
             counter += 1
             
    return counter
     

def apply_network_mask(host_address,netmask):
    host_bin = ""
    mask_bin = ""
    for i in host_address:
           host_bin = host_bin + f"{int(i):08b}"
    for x in netmask:
           mask_bin = mask_bin + f"{int(x):08b}"
    return print(f"{int(host_bin,2) & int(mask_bin,2):032b}")

def netmask_to_wildcard_mask(netmask):
    bin_netmask = ""
    wildcard_mask = []   
    for i in netmask:
        bin_netmask = bin_netmask + f"{int(i):08b}"
    for x in bin_netmask:
        if x == "1":
            wildcard_mask += "0"
        if x == "0":
            wildcard_mask += "1"
    wild_mask = ''.join(wildcard_mask)
    n = 8
    return [wild_mask[q:q+n] for q in range(0, len(wild_mask), n)]


def get_broadcast_address(network_address,wildcard_mask):
    net_bin = ""
    wild_bin = ""
    for i in network_address:
           net_bin = net_bin + f"{int(i):08b}"
    for x in wildcard_mask:
           wild_bin = net_bin + f"{int(x):08b}"
    return print(f"{int(net_bin,2) | int(wild_bin,2):032b}")

def prefix_length_to_max_hosts(subnet_length):
    host = 32
    host_max = (pow(2,host) -2)
    return host_max


